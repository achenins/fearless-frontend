import React from 'react';

class ConferenceForm extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            name: '',
            starts: '',
            ends: '',
            description: '',
            max_presentations: '',
            max_attendees: '',
            location: '',
            locations: []
        };
        this.handleInputChange = this.handleInputChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    async componentDidMount() {
        const url = 'http://localhost:8000/api/locations/';

        const response = await fetch(url);

        if (response.ok) {
          const data = await response.json();

          this.setState({locations: data.locations});

        }
      }
      
    handleInputChange(event) {
        const value = event.target.value;
        const name = event.target.name;
        this.setState({[name]: value})
    }

    async handleSubmit(event) {
        event.preventDefault();
        const data = {...this.state};
        // data.max_presentations = data.maxPresentations;
        // data.max_attendees = data.maxAttendees;
        // delete data.maxPresentations;
        // delete data.maxAttendees;
        delete data.locations;
        console.log(data);

        const conferenceUrl = 'http://localhost:8000/api/conferences/';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const response = await fetch(conferenceUrl, fetchConfig);
        if (response.ok) {
            const newConference = await response.json();
            console.log(newConference);

            const cleared = {
                name: '',
                starts: '',
                ends: '',
                description: '',
                max_presentations: '',
                max_attendees: '',
                location: '',
              };
              this.setState(cleared);
        }
    }



    render() {
        return (
            <div className="row">
                <div className="offset-3 col-6">
                    <div className="shadow p-4 mt-4">
                        <h1>Create a new conference</h1>
                        <form onSubmit={this.handleSubmit} id="create-conference-form">
                            <div className="form-floating mb-3">
                                <input onChange={this.handleInputChange} placeholder="Name" required
                                type="text" name ="name" id="name"
                                className="form-control" value={this.state.name} />
                                <label htmlFor="name">Name</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input onChange={this.handleInputChange} required
                                type="date" name="starts"  id="starts"
                                className="form-control" value={this.state.starts} />
                                <label htmlFor="starts">Starts</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input onChange={this.handleInputChange} required
                                type="date" name="ends"  id="ends"
                                className="form-control" value={this.state.ends} />
                                <label htmlFor="ends">Ends</label>
                            </div>
                            <div className="mb-3">
                                <label htmlFor="description" className="form-label">Description</label>
                                <textarea onChange={this.handleInputChange} className="form-control"
                                name="description" id="description" rows="3" value={this.state.description} ></textarea>
                            </div>
                            <div className="form-floating mb-3">
                                <input onChange={this.handleInputChange} placeholder="Maximum presentations"
                                required type="number" name="max_presentations"  id="max_presentations"
                                className="form-control" value={this.state.max_presentations} />
                                <label htmlFor="max_presentations">Maximum presentations</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input onChange={this.handleInputChange} placeholder="Maximum attendees"
                                required type="number" name="max_attendees"  id="max_attendees"
                                className="form-control" value={this.state.max_attendees} />
                                <label htmlFor="max_attendees">Maximum attendees</label>
                            </div>
                            <div className="mb-3">
                                <select onChange={this.handleInputChange} required
                                name="location" id="location"
                                className="form-select" value={this.state.location} >
                                    <option value="">Choose a location</option>
                                    {this.state.locations.map(location => {
                                        return (
                                            <option key={location.id} value={location.id}>
                                                {location.name}
                                            </option>
                                        );
                                    })}
                                </select>
                            </div>
                            <button type="submit" className="btn btn-primary">Create</button>
                        </form>
                    </div>
                </div>
            </div>
        );
    }
}

export default ConferenceForm;
